package conexionHibernate;

import javax.persistence.*;

@Entity
@Table(name="clientes")	//mapeo en una tabla llamada clientes
public class Clientes {

	//se crean los dos constructores
	
	public Clientes(String nombre, String apellidos, String direccion) {	
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.direccion = direccion;
	}

	public Clientes() {
	}

	
	//se crea el metodo toString
	
	@Override
	public String toString() {
		return "Clientes [id=" + id + ", nombre=" + nombre + ", apellidos=" + apellidos + ", direccion=" + direccion
				+ "]";
	}

	//se crean los getters y setters
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	
	//se crean las variables de las columnas
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)//vid50 asignar clave principal
	@Column(name="Id")
	private int id;
	
	@Column(name="Nombre")
	private String nombre;
	
	@Column(name="Apellidos")
	private String apellidos;
	
	@Column(name="Direccion")
	private String direccion;
	

}
