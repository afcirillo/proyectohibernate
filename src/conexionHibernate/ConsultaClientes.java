package conexionHibernate;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class ConsultaClientes {

	public static void main(String[] args) {
		
		SessionFactory miFactory=new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Clientes.class).buildSessionFactory();

		Session miSession=miFactory.openSession();
		
		try {
			
			//comenzar secion
			
			miSession.beginTransaction();
			
			//consulta de Clientes
			
			List<Clientes> losClientes=miSession.createQuery("from Clientes").getResultList();//agarra toda la lista
			
			//mostrar los clientes
			recorreClientesConsultados(losClientes);
			
			//consulta: dame los Alvarez
			
			losClientes=miSession.createQuery("from Clientes cl where cl.apellidos='Alvarez'").getResultList();
			
			//mostrar los Alvarez
			recorreClientesConsultados(losClientes);
			
			
			//mostrar los que viven en qcyo o se apellidan Cirillo
			
			losClientes=miSession.createQuery("from Clientes cl where cl.direccion='qcyo'" + 
			" or cl.apellidos='Cirillo'").getResultList();

			//mostrar 
			recorreClientesConsultados(losClientes);
			
			
			//commit
			
			miSession.getTransaction().commit();
			
			//cerrar sesion
			
			miSession.close();
			
		}finally {
			miFactory.close();

		}
		
	}

	private static void recorreClientesConsultados(List<Clientes> losClientes) {
		for(Clientes unCliente: losClientes) {
			System.out.println(unCliente);
		}
	}

}
