package conexionHibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class ActualizaClientes {

	public static void main(String[] args) {
		
		SessionFactory miFactory=new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Clientes.class).buildSessionFactory();
		
		Session miSession=miFactory.openSession();
		
		try {
			//UPDATE version1 con codigo
			/*int ClienteId=1;//necesito el id del cliente a modfificar
			
			miSession.beginTransaction();
			
			Clientes miCliente=miSession.get(Clientes.class, ClienteId);//almaceno el cliente q quiero modificar
			
			miCliente.setNombre("Santi");
			
			miCliente.setApellidos("Huck");
			
			miCliente.setDireccion("LomaDelQ");
			
			
			//UPDATE version2 con HQL
			miSession.beginTransaction();
			
			miSession.createQuery("update Clientes set Apellidos='SEVEN' where Apellidos LIKE 'A%'").executeUpdate();
			*/
			
			//DELETE con HQL
			miSession.beginTransaction();
			
			miSession.createQuery("delete Clientes where Direccion='fefeg h'").executeUpdate();
			
			miSession.getTransaction().commit();
			
			System.out.println("Regsitro actualizado correctamente en BBDD");
			
			
			
			miSession.close();


			
			
		}finally {
			miFactory.close();
		}
		
	}

}
